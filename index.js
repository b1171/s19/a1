let getCube = 4**3;
console.log(`The cube of 4 is ${getCube}.`);

const address = ["Zamboanga City", "Zamboanga del Sur", "Philippines", 7000];

const [addCity, addProvince, addCountry, addZip] = address;

console.log(`I live at ${addCity}, ${addProvince}, ${addCountry} ${addZip}.`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weigh: 1075,
	measurement: "20 ft 3 in",
}

const {name, type, weigh, measurement} = animal;

console.log(`${name} was a ${type}. He weighed at ${weigh} kgs
	with a measurement of ${measurement}.`);

let arrayNum = [1,2,3,4,5];

arrayNum.forEach((x) => console.log(x));

let reduceNumber = arrayNum.reduce((x,y) => (x+y));
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);

